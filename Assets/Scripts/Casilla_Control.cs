using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Casilla_Control : MonoBehaviour
{
    public bool piezaCorrecta = false;
    public int numeroEsperado;

    private void OnTriggerEnter(Collider other)
    {
        if (other.CompareTag("Pieza"))
        {
            int LayerIgnoreRaycast = LayerMask.NameToLayer("Ignore Raycast");
            gameObject.layer = LayerIgnoreRaycast;
            other.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;
            if (other.GetComponent<PiezaSudoku_Control>().numeroDePieza == numeroEsperado)
            {
                piezaCorrecta = true;
            }
        }
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.CompareTag("Pieza"))
        {
            int LayerDefault = LayerMask.NameToLayer("Default");
            gameObject.layer = LayerDefault;
            if (other.GetComponent<PiezaSudoku_Control>().numeroDePieza == numeroEsperado)
            {
                piezaCorrecta = false;
            }
        }
    }
}
