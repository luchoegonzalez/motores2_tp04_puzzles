using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Conexion_Control : MonoBehaviour
{
    public bool conectado = false;

    private void OnTriggerExit(Collider collision)
    {
        if (collision.gameObject.CompareTag("Cable"))
        {
            conectado = false;
        }
    }
    private void OnTriggerEnter(Collider collision)
    {
        if(collision.gameObject.CompareTag("Cable"))
        {
            conectado = true;
        }
    }
}
