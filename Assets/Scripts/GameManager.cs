using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour
{
    public Material materialVerde;
    public Material materialRojo;
    public static Material emisionRojo;
    public static Material emisionVerde;

    public List<PanelConexion_Control> panelesDeConexion;
    public GameObject cubo1;
    bool cubo1Destruido = false;

    public List<Casilla_Control> casillasSudoku;
    public GameObject cubo3;
    bool cubo3Destruido = false;

    public GameObject esferaGameOver;
    public GameObject textoGameOver;
    public GameObject jugador;

    private void Awake()
    {
        emisionRojo = materialRojo;
        emisionVerde = materialVerde;
    }

    // Update is called once per frame
    void Update()
    {
        if (TodosLosPanelesConectados() && !cubo1Destruido)
        {
            Destroy(cubo1.gameObject);
            cubo1Destruido = true;
        }

        if (CasillasCompletadas() && !cubo3Destruido)
        {
            Destroy(cubo3.gameObject);
            esferaGameOver.SetActive(true);
            cubo3Destruido = true;
        }

        if (Input.GetKeyDown(KeyCode.R))
        {
            SceneManager.LoadScene(0);
        }

        textoGameOver.transform.LookAt(jugador.transform);
        textoGameOver.transform.Rotate(new Vector3(0, 180,0), Space.Self);
    }

    private bool TodosLosPanelesConectados()
    {
        foreach(PanelConexion_Control panel in panelesDeConexion)
        {
            if (panel.conectadoCorrectamente == false)
            {
                return false;
            }
        }
        return true;
    }

    private bool CasillasCompletadas()
    {
        foreach (Casilla_Control casilla in casillasSudoku)
        {
            if (casilla.piezaCorrecta == false)
            {
                return false;
            }
        }
        return true;
    }
}
