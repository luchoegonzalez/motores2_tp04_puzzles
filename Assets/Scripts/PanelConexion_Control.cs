using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PanelConexion_Control : MonoBehaviour
{
    public List<GameObject> conexiones;
    private Renderer led;
    public bool conectadoCorrectamente = false;

    private void Start()
    {
        led = transform.GetChild(0).GetComponent<Renderer>();
    }
    private void Update()
    {
        if (ChequeoConexiones())
        {
            led.material = GameManager.emisionVerde;
            conectadoCorrectamente = true;
        } else
        {
            led.material = GameManager.emisionRojo;
            conectadoCorrectamente = false;
        }
    }

    private bool ChequeoConexiones()
    {
        foreach (GameObject conexion in conexiones)
        {
            if (conexion.GetComponent<Conexion_Control>().conectado == false)
            {
                return false;
            }
        }
        return true;
    }
}
