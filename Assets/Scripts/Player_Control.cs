using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class Player_Control : MonoBehaviour
{
    public float rapidezDesplazamiento = 10.0f;
    Camera camaraPrimeraPersona;
    public GameObject cubo2;

    bool piezaRecogida = false;
    public Transform piezasSueltas;
    public LayerMask defaultLayer;
    int layerIgnoreRaycast;

    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
        camaraPrimeraPersona = transform.GetChild(0).GetComponent<Camera>();
        layerIgnoreRaycast = LayerMask.NameToLayer("Ignore Raycast");
    }
    void Update()
    {
        float movimientoAdelanteAtras = Input.GetAxis("Vertical") * rapidezDesplazamiento;
        float movimientoCostados = Input.GetAxis("Horizontal") * rapidezDesplazamiento;
        movimientoAdelanteAtras *= Time.deltaTime;
        movimientoCostados *= Time.deltaTime;
        transform.Translate(movimientoCostados, 0, movimientoAdelanteAtras);
        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
        }

        if(Input.GetMouseButtonDown(0))
{
            Ray ray = camaraPrimeraPersona.ViewportPointToRay(new Vector3(0.5f, 0.5f, 0));
            RaycastHit hit;
            if ((Physics.Raycast(ray, out hit, 10, defaultLayer) == true) && hit.distance < 5)
            {
                if (hit.collider.CompareTag("Conexion"))
                {
                    hit.collider.transform.Rotate(new Vector3(0, 0, 90));
                }


                if (hit.collider.CompareTag("Correcto"))
                {
                    Destroy(cubo2.gameObject);
                }
                if (hit.collider.CompareTag("Incorrecto"))
                {
                    SceneManager.LoadScene(0);
                }


                if (hit.collider.CompareTag("Pieza") && !piezaRecogida)
                {
                    piezaRecogida = true;
                    hit.collider.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

                    hit.collider.gameObject.transform.position = camaraPrimeraPersona.transform.position;
                    hit.collider.gameObject.transform.SetParent(camaraPrimeraPersona.transform);
                    hit.collider.gameObject.transform.localPosition += new Vector3(0,0,1);
                    hit.collider.gameObject.transform.LookAt(camaraPrimeraPersona.transform);
                    hit.collider.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);

                    //hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    hit.collider.gameObject.layer = layerIgnoreRaycast;
                } else if (piezaRecogida && hit.collider.CompareTag("Pieza"))
                {
                    //transform.GetChild(0).GetChild(0).GetComponent<BoxCollider>().enabled = true;
                    transform.GetChild(0).GetChild(0).gameObject.layer = default;
                    transform.GetChild(0).GetChild(0).GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePositionY;

                    transform.GetChild(0).GetChild(0).SetParent(piezasSueltas);
                    hit.collider.gameObject.GetComponent<Rigidbody>().constraints = RigidbodyConstraints.FreezeAll;

                    hit.collider.gameObject.transform.position = camaraPrimeraPersona.transform.position;
                    hit.collider.gameObject.transform.SetParent(camaraPrimeraPersona.transform);
                    hit.collider.gameObject.transform.localPosition += new Vector3(0, 0, 1);
                    hit.collider.gameObject.transform.LookAt(camaraPrimeraPersona.transform);
                    hit.collider.gameObject.transform.Rotate(new Vector3(0, 180, 0), Space.Self);

                    //hit.collider.gameObject.GetComponent<BoxCollider>().enabled = false;
                    hit.collider.gameObject.layer = layerIgnoreRaycast;
                }
                else if (piezaRecogida && hit.collider.CompareTag("Casilla"))
                {
                    piezaRecogida = false;
                    //transform.GetChild(0).GetChild(0).GetComponent<BoxCollider>().enabled = true;
                    transform.GetChild(0).GetChild(0).gameObject.layer = default;
                    transform.GetChild(0).GetChild(0).GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePositionY;

                    transform.GetChild(0).GetChild(0).transform.position = hit.collider.transform.position;
                    transform.GetChild(0).GetChild(0).transform.rotation = hit.collider.transform.rotation;
                    transform.GetChild(0).GetChild(0).SetParent(hit.collider.transform);
                }
                else if (piezaRecogida)
                {
                    piezaRecogida = false;
                    //transform.GetChild(0).GetChild(0).GetComponent<BoxCollider>().enabled = true;
                    transform.GetChild(0).GetChild(0).gameObject.layer = default;
                    transform.GetChild(0).GetChild(0).GetComponent<Rigidbody>().constraints &= ~RigidbodyConstraints.FreezePositionY;

                    transform.GetChild(0).GetChild(0).SetParent(piezasSueltas);
                }

            }
        }
    }
}